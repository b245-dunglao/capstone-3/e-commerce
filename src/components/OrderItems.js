import {Table, Button} from 'react-bootstrap';
import {Link, useNavigate} from 'react-router-dom';
import {BsEye} from  'react-icons/bs'
import {Fragment} from 'react';



import {useState} from 'react';




export default function OrderItems({orderItemsProp}) {

  const navigate = useNavigate();
  
  const{_id, totalAmount, completed} = orderItemsProp;

  return (
    
        
        <tr>
          <td>{_id}</td>
          <td>{totalAmount}</td>

          {completed ? (
            <Fragment>
              <td>Completed</td>
              <td>
                <BsEye onClick={()=>navigate(`/vieworder/${_id}`)}/>
              </td>
            </Fragment>
          ) : (
            <Fragment>
              <td>Active</td>
              <td>
              </td>
            </Fragment>
          )}
        </tr>
      
  );
}















