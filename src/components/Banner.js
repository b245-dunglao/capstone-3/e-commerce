import { Row , Col , Button } from 'react-bootstrap';
import {Link} from 'react-router-dom';


export default function	Banner (){
	return (
		
			<Row className="my-5">
				<Col className = "text-center">
					<h1> ebooks </h1>
					<p className ="pt-1"> Read anywhere, anytime, with ease  </p>
					<Button as = {Link} to ='/productsearch' className ="pt-1 shadow btn-secondary"> Check Products </Button>
				</Col>
			</Row >
		

		)
}