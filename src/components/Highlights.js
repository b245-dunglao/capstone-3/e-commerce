import { Card, CardBody, Row, Col, Button} from 'react-bootstrap';
import {useState, useRef, useEffect} from 'react'


export default function	Highlights(){

	const [showMore, setShowMore] = useState(false);
	const [maxHeight, setMaxHeight] = useState('none');
	const [contentHeight, setContentHeight] = useState(0);
  const contentRef = useRef(null);

   useEffect(() => {
    if (contentRef.current) {
      const contentHeight = contentRef.current.offsetHeight;
      setMaxHeight(contentHeight + 'px');
    }
  }, []);

   const toggleHeight = () => {
    setShowMore(!showMore);
    setMaxHeight(maxHeight === 'none' ? contentRef.current.offsetHeight + 'px' : 'none');
  };

	return (
			<Row>
				<Col className =" m-1 mx-auto mt-5">
					<Card className= " mt-5" >
						<Card.Img variant="top" src="https://www.publishersweekly.com/images/cached/ARTICLE_PHOTO/photo/000/000/073/73915-v1-600x.JPG" />
					    <Card.Body ref={contentRef}>
					      <Card.Title>Publishing Leaders Issuing Warning over Amazon’s Market Power</Card.Title>
					       <Card.Text>
					        By Jim Milliot | Aug 18, 2020
					      </Card.Text>
					      <Card.Text>
					        Three of publishing’s most important organizations have teamed up to write a letter to the chairman of the House Antitrust Subcommittee investigating the market power of Big Tech to press their case that, over the last several years, Amazon’s growing dominance over book publishing and bookselling has fundamentally altered the competitive framework of the industry. If Amazon’s power is left unchecked, the letter continues, competition within publishing could diminish even more.
					        </Card.Text>
					        <Card.Text>
					        In a joint letter to Rep. David Cicilline (D-R.I.), Maria Pallante, president and CEO of the Association of American Publishers, Mary Rasenberger, executive director of the Authors Guild, and Allison Hill, CEO of the American Booksellers Association, wrote that their members have long relied on a level playing field to publish and sell their works. But today, the letter continued, “Amazon no longer competes on a level playing field when it comes to book distribution, but, rather, owns and manipulates the playing field, leveraging practices from across its platform that appear to be well outside of fair and transparent competition.”
					        </Card.Text>
					        <Card.Text>
					        Sorce: https://www.publishersweekly.com/pw/by-topic/industry-news/bookselling/article/84119-publishing-leaders-issuing-warning-over-amazon-s-market-power.html
					      </Card.Text>
					    </Card.Body>
					 </Card>
					 <Card className= " mt-5">
					 		<Card.Img variant="top" src="https://www.jkrowling.com/wp-content/uploads/2020/05/JKR.com_-2.jpg" />
					 							    <Card.Body ref={contentRef}>
					 							      <Card.Title>J.K. Rowling just announced a new children's book — and you can already read the first two chapters online</Card.Title>
					 							       <Card.Text>
					 							        BY DANIELLE GARRAND
					 							      </Card.Text>
					 							      <Card.Text>
					 							        Rowling broke the news on Twitter, telling fans the book is not a spin-off of her best-selling "Harry Potter" series, but a brand new story. Rowling said in a press release on her website that she wrote "most of a first draft" more than 10 years ago, while she was still writing the "Harry Potter" books.
					 							        </Card.Text>
					 							        <Card.Text>
					 							        "I always meant to publish it, but after the last Potter was released I wrote two novels for adults and, after some dithering, decided to put those out next," Rowling tweeted, adding, "until very recently, the only people who'd heard the story of The Ickabog were my two younger children."
					 							        </Card.Text>
					 							        <Card.Text>
					 							        Sorce: https://www.cbsnews.com/news/j-k-rowling-announces-the-ickabog-new-childrens-book/ | https://www.jkrowling.com/j-k-rowling-introduces-the-ickabog/
					 							      </Card.Text>
					 							    </Card.Body>
					   </Card>

				</Col>
			</Row>
		)
}
