import {Fragment} from 'react';
import {Container} from 'react-bootstrap';

import Banner from '../components/Banner.js'
import Highlights from '../components/Highlights.js'



export default function home () {
	
	return (
		<Fragment>
			<Container >	
				<Banner/>
			</Container>
			<Container>
				<Highlights/>
			</Container>
		</Fragment>
		)

}