import {Button, Form, Row, Col, Table, Container} from 'react-bootstrap';
import {Fragment, useContext, useState, useEffect} from 'react';
import {Navigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import OrderItems from '../components/OrderItems.js'
import {AiOutlineEye} from 'react-icons/ai'

import '../index.css';

import Products from '../components/Products.js'


import UserContext from '../UserContext.js';


export default function Orders() {

  const [name,setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');
  const [imgLink, setImgLink] = useState('');
  const [isHidden, setIsHidden] = useState(true);
  const [orders, setOrders] = useState([]);


  

  const {user} = useContext(UserContext);

  const [isActive, setIsActive] = useState(false);


  const[products, setProducts] = useState([]);

  useEffect(()=>{

    fetch(`${process.env.REACT_APP_API_URL}/order/reviewAll/all`,{
      method: 'GET',
      headers:{
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    }).then(result=>result.json())
    .then(data => {
      setOrders(data.map(order=>{
        return (<OrderItems key ={order._id} orderItemsProp = {order}/>)
      }))
    })
   
  }, [])


  return (
  
    user && !user.isAdmin ? 

    <Fragment>  
      <Container className="d-none d-lg-block">
            <Table responsive="sm" striped bordered hover className="mt-5 shadow">
                 <thead>
                   <tr>
                     <th>Order ID</th>
                     <th>Total Amount</th>
                     <th>Completed</th>
                     <th>Action</th>
                   </tr>
                 </thead>
                 <tbody>
                    {orders}
                 </tbody>
              </Table>
      </Container>
    </Fragment>

    
    :

    <Navigate to = "/"/>

    
  );
}
