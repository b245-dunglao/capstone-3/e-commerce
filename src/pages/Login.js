
import {Button, Form, Container, Row, Col} from 'react-bootstrap';
import {Fragment, useState, useContext, useEffect} from 'react';
import {Navigate, useNavigate} from 'react-router-dom';

import UserContext from '../UserContext.js';


import Swal from 'sweetalert2';



export default function Login () {
	const [email,setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);
	const {user, setUser} = useContext(UserContext);
	

	const navigate = useNavigate();

	



	useEffect(() =>{
		
		if (email !== "" && password !== ""){
			setIsActive(false);
		}
		else{
			setIsActive(true);
		}
	}, [email,password])




	function login (event){

		event.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/user/login`,{
			method: 'POST',
			headers:{
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email:email,
				password: password
			})
		}).then(result=>result.json())
		.then(data=>{

			if(data === false){
				
				Swal.fire({
					title: "Authentication failed.",
					icon: "error",
					text: "Your credentials are wrong. You may register your account to our website."
				})
			}else{
				localStorage.setItem('token', data.auth);
				retrieveUserDetails(localStorage.getItem('token'));
				

				Swal.fire({
					title: "Authentication successful!",
					icon: "success",
					text: "Welcome to ebook"
				})
					if (!data.isAdmin){
						//check recorded order
						fetch(`${process.env.REACT_APP_API_URL}/order/checkOut`,{
							method: 'POST',
							headers:{
								'Content-Type': 'application/json',
								Authorization: `Bearer ${localStorage.getItem('token')}`
							}
						}).then(result=>result.json())
						.then(data=>{
							localStorage.setItem('orderId', data._id);
							

						})
					}
					else{

					}



				
				navigate('/');
			}
		})


		setEmail('');
		setPassword('');

	}

	const retrieveUserDetails = (token) => {

		fetch(`${process.env.REACT_APP_API_URL}/user/account`, {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(data => {
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})

	}
	
	return (
		user ?
		<Navigate to = "/*"/>
		:
		
		<Fragment>
			<Row className="mt-5">
				<Col className="col-md-7 col-10 mx-auto p-3">
					<Container className="my-5">
					<h4 className ="text-center mt-2 text-muted mb-5" >Together we view things differently.</h4>
					<Form className = "my-3" onSubmit ={event =>login(event)}>
					      <Form.Group className="mb-3" controlId="formBasicEmail">
					        <Form.Label>Email address</Form.Label>
					        <Form.Control 
					        type="email" 
					        placeholder="Enter email" 
					        value ={email} 
					        onChange ={event=> setEmail(event.target.value)}
					        required
					        className="shadow-sm"/>
					      </Form.Group>
					      <Form.Group className="mb-3" controlId="formBasicPassword">
					        <Form.Label>Password</Form.Label>
					        <Form.Control 
					        type="password" 
					        placeholder="Password" 
					        value ={password} 
					        onChange ={event=> setPassword(event.target.value)}
					        required
					        className="shadow-sm"/>
					      </Form.Group>
					      <Form.Group className="mb-2 text text-center">
					        <Form.Text className="text-underline" onClick={()=>{Swal.fire({
					title: "Contact Us.",
					icon: "info",
					text: "For your username and password concerns, please contact us at bookz@mail.com"
				})}}><u>Forgot Username or Password?</u>
					        </Form.Text>
					      </Form.Group>
					      <Form.Group className="mb-2 text text-center">
					      <Button className="btn-secondary shadow"variant="primary" type="submit" disabled={isActive}>
					        Sign In
					      </Button>
					      </Form.Group>
					    </Form>
					</Container>
				
				</Col>
			</Row>
		</Fragment>
		
	)
}

